namespace EVEMon.Common.Enumerations
{
    /// <summary>
    /// Enumeration of an HTTP method.
    /// </summary>
    public enum HttpMethod
    {
        Get,
        Post,
        Postentity,
        Put
    }
}