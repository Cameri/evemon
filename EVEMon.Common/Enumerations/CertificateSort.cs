﻿namespace EVEMon.Common.Enumerations
{
    public enum CertificateSort
    {
        Name = 0,
        TimeToNextLevel = 1,
        TimeToMaxLevel = 2
    }
}
